﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using TrippyTrips.Repositories.Repositories.City;

namespace TrippyTrips.MSSQL.Repositories.City
{
    public class CityRepository : Repository<TrippyTrips.Repositories.Entities.City>, ICityRepository
    {
        private TrippyTripsContext _context;

        public CityRepository(TrippyTripsContext context) : base(context)
        {
            _context = context;
        }

        public async Task<TrippyTrips.Repositories.Entities.City> Get(int id)
        {
            return await base.Get(id)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.Id == id);
        }

        public async Task Add(TrippyTrips.Repositories.Entities.City entity)
        {
            await base.Insert(entity);
        }
    }
}
