﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TrippyTrips.Repositories.Entities;
using TrippyTrips.Repositories.Repositories.Common;

namespace TrippyTrips.MSSQL.Repositories
{
    public class LanguageRepository : Repository<Language>, ILanguageRepository
    {
        private TrippyTripsContext _context;

        public LanguageRepository(TrippyTripsContext context) : base(context)
        {
            _context = context;
        }

        public IReadOnlyList<Language> GetAll()
        {
            return base.GetAll().AsNoTracking().ToList();
        }
    }
}
