﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TrippyTrips.Repositories.Entities;

namespace TrippyTrips.MSSQL.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        DbSet<T> GetAll();

        DbSet<T> Get(long id);

        Task Insert(T entity);

        Task Update(T entity);

        Task Delete(T entity);

        void Remove(T entity);

        Task SaveChangesAsync();
    }
}
