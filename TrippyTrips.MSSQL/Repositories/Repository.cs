﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrippyTrips.Repositories.Entities;

namespace TrippyTrips.MSSQL.Repositories
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly TrippyTripsContext _context;
        private DbSet<T> _entities;
        string errorMessage = string.Empty;

        public Repository(TrippyTripsContext context)
        {
            _context = context;
            _entities = context.Set<T>();
        }
        public DbSet<T> GetAll()
        {
            return _entities;
        }

        public DbSet<T> Get(long id)
        {
            return _entities;
        }

        public async Task Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            await _entities.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            await _context.SaveChangesAsync();
        }

        public async Task Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _entities.Remove(entity);
            await _context.SaveChangesAsync();
        }
        public void Remove(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _entities.Remove(entity);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
