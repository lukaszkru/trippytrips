﻿using Microsoft.EntityFrameworkCore;
using TrippyTrips.Repositories.Entities;

namespace TrippyTrips.MSSQL
{
    public class TrippyTripsContext : DbContext
    {
        public TrippyTripsContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<City> Cities { get; set; }

        public DbSet<Country> Countries { get; set; }

        //public DbSet<Category> Categories { get; set; }

       // public DbSet<Language> Languages { get; set; }

        //public DbSet<Monument> Monuments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Language>()
            //    .ToTable(nameof(Language))
            //    .HasKey(l => l.Id);
            
            //modelBuilder.Entity<Category>()
            //    .ToTable(nameof(Category))
            //    .HasKey(c => c.Id);

            modelBuilder.Entity<Country>().ToTable(nameof(Country));
            modelBuilder.Entity<City>().ToTable(nameof(City));

            //modelBuilder.Entity<Monument>()
            //    .ToTable(nameof(Monument))
            //    .HasKey(m => m.Id);

        }
    }
}
