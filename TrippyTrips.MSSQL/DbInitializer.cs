﻿using System;
using System.Linq;
using TrippyTrips.Repositories.Entities;

namespace TrippyTrips.MSSQL
{
    public static class DbInitializer
    {
        public static void Initialize(TrippyTripsContext context)
        {
            context.Database.EnsureCreated();

            if (context.Countries.Any())
            {
                return;   // DB has been seeded
            }

            var countries = new Country[]
            {
                new Country{Name="Poland", CreateDate=DateTime.UtcNow},
            };

            foreach (var c in countries)
            {
                context.Countries.Add(c);
            }
            context.SaveChanges();

            var cities = new City[]
            {
                new City{Name="Warsaw", IsActive=true, CreateDate=DateTime.UtcNow, CountryId = 1 },
                new City{Name="Bialystok", IsActive=false, CreateDate=DateTime.UtcNow, CountryId = 1 },
            };
            foreach (var c in cities)
            {
                context.Cities.Add(c);
            }
            context.SaveChanges();

            //var languages = new Language[]
            //{
            //    new Language{Code="PL", LanguageCulture="PL-pl", Name="Polish", CreateDate=DateTime.UtcNow},
            //};

            //foreach (var l in languages)
            //{
            //    context.Languages.Add(l);
            //}
            //context.SaveChanges();

    //        var categories = new Category[]
    //        {
    //            new Category{Name="Walk trip", CreateDate=DateTime.UtcNow},
    //        };

    //        foreach (var c in categories)
    //        {
    //            context.Categories.Add(c);
    //        }
    //        context.SaveChanges();
        }
    }
}
