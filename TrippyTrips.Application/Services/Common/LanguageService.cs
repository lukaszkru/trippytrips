﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using TrippyTrips.Contract.Dto.Common;
using TrippyTrips.Contract.Interfaces.Common;
using TrippyTrips.Repositories.Repositories.Common;

namespace TrippyTrips.Application.Services.Common
{
    public class LanguageService : ILanguageService
    {
        private readonly IMapper _mapper;
        private readonly ILanguageRepository _languageRepository;

        public LanguageService(
            IMapper mapper,
            ILanguageRepository languageRepository)
        {
            _mapper = mapper;
            _languageRepository = languageRepository;
        }

        public IReadOnlyList<Language> GetAllLanguages()
        {
            var languages = _languageRepository.GetAll();
            return _mapper.Map<IReadOnlyList<Repositories.Entities.Language>, IReadOnlyList<Language>>(languages);
        }
    }
}
