﻿using AutoMapper;
using System.Threading.Tasks;
using TrippyTrips.Contract.Interfaces.City;
using TrippyTrips.Repositories.Repositories.City;

namespace TrippyTrips.Application.Services.City
{
    public class CityService : ICityService
    {
        private readonly IMapper _mapper;
        private readonly ICityRepository _cityRepository;
        
        public CityService(
            IMapper mapper,
            ICityRepository cityRepository)
        {
            _mapper = mapper;
            _cityRepository = cityRepository;
        }

        async Task<Contract.Dto.City.City> ICityService.GetCity(int id)
        {
            var city = await _cityRepository.Get(id);
            return _mapper.Map<Repositories.Entities.City, Contract.Dto.City.City>(city);
        }

        async Task ICityService.AddCity(Contract.Dto.City.City city)
        {
            var entity = _mapper.Map<Contract.Dto.City.City, Repositories.Entities.City>(city);
            await _cityRepository.Add(entity);
        }
    }
}
