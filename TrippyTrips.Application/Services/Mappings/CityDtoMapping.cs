﻿using AutoMapper;
using System;

namespace TrippyTrips.Application.Services.Mappings
{
    public class CityDtoMapping : Profile
    {
        public CityDtoMapping()
        {
            CreateMap<Repositories.Entities.City, Contract.Dto.City.City>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.CreateDate, opt => opt.MapFrom(x => x.CreateDate))
                .ForMember(x => x.CountryId, opt => opt.MapFrom(x => x.CountryId));

            CreateMap<Contract.Dto.City.City, Repositories.Entities.City>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.IsActive, opt => opt.MapFrom(x => x.IsActive))
                .ForMember(x => x.Country, opt => opt.MapFrom(x => x.Country))
                .ForMember(x => x.CountryId, opt => opt.MapFrom(x => x.CountryId))
                .ForMember(x => x.CreateDate, opt => opt.MapFrom(x => x.CreateDate));
                //.ForMember(x => x.ModificationDate, opt => opt.UseValue(DateTime.UtcNow));
        }
    }
}
