﻿using AutoMapper;
using TrippyTrips.Contract.Dto.Common;

namespace TrippyTrips.Application.Services.Mappings
{
    public class LanguageDtoMapping : Profile
    {
        public LanguageDtoMapping()
        {
            CreateMap<Repositories.Entities.Language, Language>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.Code, opt => opt.MapFrom(x => x.Code))
                .ForMember(x => x.LanguageCulture, opt => opt.MapFrom(x => x.LanguageCulture));
        }
    }
}
