﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrippyTrips.Application.Services.Mappings
{
    public class CountryDtoMapping : Profile
    {
        public CountryDtoMapping()
        {
            CreateMap<Contract.Dto.City.City, Repositories.Entities.City>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.CreateDate, opt => opt.MapFrom(x => x.CreateDate));
        }
    }
}
