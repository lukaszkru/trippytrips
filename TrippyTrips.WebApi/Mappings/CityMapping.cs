﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrippyTrips.WebApi.ViewModels.City;

namespace TrippyTrips.WebApi.Mappings
{
    public class CityMapping : Profile
    {
        public CityMapping()
        {
            CreateMap<Contract.Dto.City.City, CityViewModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.CreateDate, opt => opt.MapFrom(x => x.CreateDate))
                .ForMember(x => x.CountryId, opt => opt.MapFrom(x => x.CountryId));
            
            CreateMap<CityViewModel, Contract.Dto.City.City>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.CreateDate, opt => opt.MapFrom(x => x.CreateDate))
                .ForMember(x => x.CountryId, opt => opt.MapFrom(x => x.CountryId))
                .ForMember(x => x.IsActive, opt => opt.MapFrom(x => x.IsActive));
        }
    }
}
