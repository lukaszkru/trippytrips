﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrippyTrips.WebApi.ViewModels.Country;

namespace TrippyTrips.WebApi.Mappings
{
    public class CountryMapping : Profile
    {
        public CountryMapping()
        {
            CreateMap<Contract.Dto.Country.Country, CountryViewModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.CreateDate, opt => opt.MapFrom(x => x.CreateDate));

            CreateMap<CountryViewModel, Contract.Dto.Country.Country>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.CreateDate, opt => opt.MapFrom(x => x.CreateDate));
        }
    }
}
