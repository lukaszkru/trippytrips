﻿using AutoMapper;
using TrippyTrips.Contract.Dto.Common;
using TrippyTrips.WebApi.ViewModels.Language;

namespace TrippyTrips.WebApi.Mappings
{
    public class LanguageMapping : Profile
    {
        public LanguageMapping()
        {
            CreateMap<Language, LanguageViewModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.Code, opt => opt.MapFrom(x => x.Code))
                .ForMember(x => x.LanguageCulture, opt => opt.MapFrom(x => x.LanguageCulture))
                .ForMember(x => x.CreateDate, opt => opt.MapFrom(x => x.CreateDate));
        }
    }
}
