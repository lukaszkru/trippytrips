﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace TrippyTrips.WebApi.Controllers.Attachment
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ImageController : Controller
    {
        [HttpGet("image")]
        [SwaggerOperation(Tags = new[] { AreaTag.Attachment })]
        //[ProducesResponseType(typeof(IList<LanguageViewModel>), 200)]
        public void GetImage()
        {

        }
    }
}