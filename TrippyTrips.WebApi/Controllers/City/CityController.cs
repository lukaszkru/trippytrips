﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Swashbuckle.AspNetCore.SwaggerGen;
using TrippyTrips.WebApi.ViewModels.City;
using TrippyTrips.Contract.Interfaces.City;
using System.Threading.Tasks;

namespace TrippyTrips.WebApi.Controllers.City
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CityController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ICityService _cityService;

        public CityController(
            IMapper mapper,
            ICityService cityService)
        {
            _mapper = mapper;
            _cityService = cityService;
        }

        [HttpGet("city")]
        [SwaggerOperation(Tags = new[] { AreaTag.City })]
        [ProducesResponseType(typeof(IList<CityViewModel>), 200)]
        public async Task<CityViewModel> GetCity(int id)
        {
            var city = await _cityService.GetCity(id);
            return _mapper.Map<Contract.Dto.City.City, CityViewModel>(city);
        }

        [HttpPost("city")]
        [SwaggerOperation(Tags = new[] { AreaTag.City })]
        [ProducesResponseType(typeof(IList<CityViewModel>), 200)]
        public async Task AddCity([FromBody] CityViewModel city)
        {
            var cityDto = _mapper.Map<CityViewModel, Contract.Dto.City.City>(city);
            await _cityService.AddCity(cityDto);
        }
    }
}