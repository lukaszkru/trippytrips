﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrippyTrips.WebApi.Controllers
{
    public static class AreaTag
    {
        public const string Attachment = "Attachment";
        public const string City = "City";
        public const string Common = "Common";
    }
}
