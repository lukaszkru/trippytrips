﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;
using TrippyTrips.Contract.Dto.Common;
using TrippyTrips.Contract.Interfaces.Common;
using TrippyTrips.WebApi.ViewModels.Language;

namespace TrippyTrips.WebApi.Controllers.Common
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class LanguageController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ILanguageService _languageService;

        public LanguageController(
            IMapper mapper, 
            ILanguageService languageService)
        {
            _mapper = mapper;
            _languageService = languageService;
        }

        [HttpGet("languages")]
        [SwaggerOperation(Tags = new[] { AreaTag.Common })]
        [ProducesResponseType(typeof(IList<LanguageViewModel>), 200)]
        public IReadOnlyList<LanguageViewModel> GetAllLanguages()
        {
            var languages = _languageService.GetAllLanguages().ToList();
            return _mapper.Map<List<Language>, List<LanguageViewModel>>(languages);
        }
    }
}