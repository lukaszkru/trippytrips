﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using TrippyTrips.Application.Services.City;
using TrippyTrips.Application.Services.Common;
using TrippyTrips.Contract.Interfaces.City;
using TrippyTrips.Contract.Interfaces.Common;
using TrippyTrips.MSSQL;
using TrippyTrips.MSSQL.Repositories;
using TrippyTrips.MSSQL.Repositories.City;
using TrippyTrips.Repositories.Repositories.City;
using TrippyTrips.Repositories.Repositories.Common;

namespace TrippyTrips.WebApi
{
    public class Startup
    {
        private IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TrippyTripsContext>(options
                => options.UseSqlServer(_configuration["TrippyTripsDatabase"] ?? _configuration.GetConnectionString("TrippyTripsDatabase")));
            
            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "TrippyTrips API",
                    Version = "v1"
                });
            });

            // DI
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            RegisterInfrastructure(services);

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                c.DocExpansion(DocExpansion.None);
            });

            app.UseMvc();
        }

        private void RegisterInfrastructure(IServiceCollection services)
        {
            services.AddScoped<ILanguageService, LanguageService>();
            services.AddScoped<ILanguageRepository, LanguageRepository>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<ICityRepository, CityRepository>();
        }

    }
}
