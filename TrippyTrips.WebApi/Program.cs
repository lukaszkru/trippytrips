﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TrippyTrips.MSSQL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Azure.KeyVault;
using Microsoft.Extensions.Configuration.AzureKeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using System.IO;

namespace TrippyTrips.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<TrippyTripsContext>();

                    // Use only in development environment
                    context.Database.Migrate();
                    context.Database.EnsureCreated();

                    DbInitializer.Initialize(context);                  
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseApplicationInsights()
                .ConfigureAppConfiguration(ConfigConfiguration)
                .UseStartup<Startup>()
                .Build();
        }

        static void ConfigConfiguration(WebHostBuilderContext webHostBuilderContext, IConfigurationBuilder configurationBuilder)
        {
            if (!webHostBuilderContext.HostingEnvironment.IsDevelopment())
            {
                configurationBuilder.SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("azurekeyvault.json", false, true)
                    .AddEnvironmentVariables();

                var config = configurationBuilder.Build();

                configurationBuilder.AddAzureKeyVault(
                    $"https://{config["azureKeyVault:vault"]}.vault.azure.net/",
                    config["azureKeyVault:clientId"],
                    config["azureKeyVault:clientSecret"]
                );
            }
        }
    }
}
