﻿using System;
using TrippyTrips.WebApi.ViewModels.Country;

namespace TrippyTrips.WebApi.ViewModels.City
{
    public class CityViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public Int64 CountryId { get; set; }

        public bool IsActive { get; set; }
    }
}
