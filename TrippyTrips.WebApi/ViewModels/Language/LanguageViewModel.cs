﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrippyTrips.WebApi.ViewModels.Language
{
    public class LanguageViewModel : BaseViewModel
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string LanguageCulture { get; set; }

    }
}
