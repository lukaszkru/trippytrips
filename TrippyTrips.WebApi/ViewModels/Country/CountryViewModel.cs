﻿using System.Collections.Generic;
using TrippyTrips.WebApi.ViewModels.City;

namespace TrippyTrips.WebApi.ViewModels.Country
{
    public class CountryViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public ICollection<CityViewModel> Cities { get; set; }
    }
}
