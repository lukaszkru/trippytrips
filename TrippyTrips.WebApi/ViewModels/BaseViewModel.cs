﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrippyTrips.WebApi.ViewModels
{
    public class BaseViewModel
    {
        public Int64 Id { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
