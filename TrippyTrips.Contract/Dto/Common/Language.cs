﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrippyTrips.Contract.Dto.Common
{
    public class Language : BaseDto
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string LanguageCulture { get; set; }
    }
}
