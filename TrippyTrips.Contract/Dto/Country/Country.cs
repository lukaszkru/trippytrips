﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrippyTrips.Contract.Dto.Country
{
    public class Country : BaseDto
    {
        public string Name { get; set; }

        public ICollection<City.City> Cities { get; set; }
    }
}
