﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrippyTrips.Contract.Dto
{
    public class BaseDto
    {
        public Int64 Id { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
