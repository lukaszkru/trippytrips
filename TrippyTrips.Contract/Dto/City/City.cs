﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrippyTrips.Contract.Dto.City
{
    public class City : BaseDto
    {
        public string Name { get; set; }

        public Int64 CountryId { get; set; }

        public Country.Country Country { get; set; }

        public bool IsActive { get; set; }
    }
}
