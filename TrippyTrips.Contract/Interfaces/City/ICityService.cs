﻿using System.Threading.Tasks;

namespace TrippyTrips.Contract.Interfaces.City
{
    public interface ICityService
    {
        Task<Dto.City.City> GetCity(int id);

        Task AddCity(Dto.City.City city);
    }
}
