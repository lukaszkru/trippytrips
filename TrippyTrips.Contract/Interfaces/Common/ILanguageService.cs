﻿using System;
using System.Collections.Generic;
using System.Text;
using TrippyTrips.Contract.Dto.Common;

namespace TrippyTrips.Contract.Interfaces.Common
{
    public interface ILanguageService
    {
        IReadOnlyList<Language> GetAllLanguages();
    }
}
