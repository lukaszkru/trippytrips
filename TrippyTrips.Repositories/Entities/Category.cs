﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrippyTrips.Repositories.Entities
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }
    }
}
