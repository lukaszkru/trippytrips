﻿using System;

namespace TrippyTrips.Repositories.Entities
{
    public class BaseEntity
    {
        public Int64 Id { get; set; }

        public DateTime CreateDate { get; set; }

        //public DateTime? ModificationDate { get; set; }
    }
}
