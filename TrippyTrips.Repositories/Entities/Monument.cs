﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrippyTrips.Repositories.Entities
{
    public class Monument : BaseEntity
    {
        public string Name { get; set; }

        public City City { get; set; }
    }
}
