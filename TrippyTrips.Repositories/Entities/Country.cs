﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrippyTrips.Repositories.Entities
{
    public class Country : BaseEntity
    {        
        public string Name { get; set; }

        public ICollection<City> Cities { get; set; }
    }
}
