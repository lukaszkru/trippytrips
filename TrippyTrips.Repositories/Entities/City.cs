﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrippyTrips.Repositories.Entities
{
    public class City : BaseEntity
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public Int64 CountryId { get; set; }

        public Country Country { get; set; }
    }
}
