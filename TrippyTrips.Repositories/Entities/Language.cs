﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrippyTrips.Repositories.Entities
{
    public class Language : BaseEntity
    {
        public string Code { get; set; }
        
        public string Name { get; set; }

        public string LanguageCulture { get; set; }
    }
}
