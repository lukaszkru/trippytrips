﻿using System.Threading.Tasks;

namespace TrippyTrips.Repositories.Repositories.City
{
    public interface ICityRepository
    {
        Task<Entities.City> Get(int id);

        Task Add(Entities.City city);
    }
}
