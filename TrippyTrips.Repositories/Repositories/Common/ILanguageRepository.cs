﻿using System;
using System.Collections.Generic;
using System.Text;
using TrippyTrips.Repositories.Entities;

namespace TrippyTrips.Repositories.Repositories.Common
{
    public interface ILanguageRepository
    {
        IReadOnlyList<Language> GetAll();
    }
}
